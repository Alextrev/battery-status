#import <Cordova/CDV.h>

@interface BatteryPlugin : CDVPlugin

- (void)getStatusBattery:(CDVInvokedUrlCommand*)command;

@end