#import "BatteryPlugin.h"
#import <Cordova/CDV.h>

@implementation BatteryPlugin

- (void)getStatusBattery:(CDVInvokedUrlCommand*)command
{
CDVPluginResult* pluginResult = nil;
NSString* myarg = [command.arguments objectAtIndex:0];

if (myarg != nil) {

[[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
UIDevice *myDevice = [UIDevice currentDevice];
[myDevice setBatteryMonitoringEnabled:YES];
double batLeft = (float)[myDevice batteryLevel] * 100;
[[UIDevice currentDevice] setBatteryMonitoringEnabled:NO];

pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[NSString stringWithFormat:@"%.0f", batLeft]];
} else {
pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR    messageAsString:@"Arg was null"];
}

[self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
